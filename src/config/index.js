/**
 * 环境配置封装
 */
import tr from "element-plus/packages/locale/lang/tr";

const env = import.meta.env.MODE || 'prod';
const EnvConfig = {
    dev:{
        baseApi:'/api',
        mockApi:'https://www.fastmock.site/mock/6304a84ed2a86143973da3ce7c13290a/api'
    },
    test:{
        baseApi:'//test.futurefe.com/api',
        mockApi:'https://www.fastmock.site/mock/6304a84ed2a86143973da3ce7c13290a/api'
    },
    prod:{
        baseApi:'//futurefe.com/api',
        mockApi:'https://www.fastmock.site/mock/6304a84ed2a86143973da3ce7c13290a/api'
    }
}
/*export default {
    env:'dev',
    mock:true,
    baseApi:'www.baidu.com/api'
}*/
export default {
    env,
    mock:false,
    namespace:'manager',
    ...EnvConfig[env]
}
