import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
// import axios from 'axios'
// import config from './config'
import request from './utils/request'
import storage from './utils/storage'
import api from './api'
import store from './store'

// 获取环境变量
console.log("环境变量=>", import.meta.env);
// createApp(App).mount('#app')
const app = createApp(App);
// axios.get(config.mockApi + '/login').then((res)=>{
//     console.log(res)
// })
app.config.globalProperties.$request = request // 全局挂载
app.config.globalProperties.$api = api // 全局挂载
app.config.globalProperties.$storage = storage // 全局挂载
app.use(router).use(store).use(ElementPlus,{size:'small'}).mount('#app')
